package com.citi.trading.pricing;

import java.util.ArrayList;

public class PriceData implements Subject{
	private ArrayList<Observer> observers;
	private final Object MUTEX = new Object();
	public PriceData() {
		this.observers = new ArrayList<>();
	}
	@Override
	public void register(Observer obj) {
		if (obj == null) throw new NullPointerException("Null observer");
		synchronized (MUTEX) {
			if(!observers.contains(obj)) observers.add(obj);
		}
		
	}

	@Override
	public void unregister(Observer obj) {
		synchronized (MUTEX) {
			observers.remove(obj);
		}
		
	}

	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getUpdate(Observer obj) {
		// TODO Auto-generated method stub
		return null;
	}

}
