package com.citi.trading.pricing;

public interface Observer {
	public void update();
	public void setSubject(Subject sub);
}
